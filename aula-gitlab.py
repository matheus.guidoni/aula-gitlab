Gerador de frases aleatórias

import random

parte1 = [
        "O sistema em desenvolvimento",
        "O novo protocolo de comunicação",
        "O algoritimo otimizado"]
parte2 = [
        "possui excelente desempenho",
        "oferece garantias de precisão cima da média",
        "preenche ua  lacuna significativa"
        ]
parte3 = [
        "nas aplicaçoes a que se destina",
        "em relação àsopções disponiveis no mercado",
        "arroz pequeno"
        ]

lingua = int(input("Escolha a lingua: 1 - portugues; 2 - ingles\n"))

if lingua == 2:
    parte1 = []
    parte2 = []
    parte3 = []

print (random.choice(parte1), random.choice(parte2), random.choice(parte3))
